/**
 * @file
 */

(function ($) {

    var stopwatchId = document.getElementById('stopwatch');
    var start = document.getElementById('stopwatch_start');
    var stop = document.getElementById('stopwatch_stop');
    var clear = document.getElementById('stopwatch_clear');
    var seconds = 0;
    var minutes = 0;
    var hours = 0;
    var time;

    function add() {

        seconds++;
        if (seconds >= 60) {
            seconds = 0;
            minutes++;
            if (minutes >= 60) {
                minutes = 0;
                hours++;
            }
        }

        stopwatchId.textContent = (hours ? (hours > 9 ? hours : "0" + hours) : "00") + ":" + (minutes ? (minutes > 9 ? minutes : "0" + minutes) : "00") + ":" + (seconds > 9 ? seconds : "0" + seconds);

        timer();

    }

    function timer() {
        time = setTimeout(add, 1000);
    }

    /* Start button */
    start.onclick = timer;

    /* Stop button */
    stop.onclick = function () {
        clearTimeout(time);
    }

    /* Clear button */
    clear.onclick = function () {
        stopwatchId.textContent = "00:00:00";
        seconds = 0;
        minutes = 0;
        hours = 0;
    }

})(jQuery);
