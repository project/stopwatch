Stopwatch
==============================

This module provides a block with a stopwatch.

Installation.
=============

1. Unzip the files to the "/modules" directory and enable the module.

2. Go to the admin/structure/block page to enable the stopwatch block.

Credit
======

The module utilises the Javascript stopwatch code.
