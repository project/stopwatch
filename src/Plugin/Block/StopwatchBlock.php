<?php
/**
 * @file Drupal\stopwatch\Plugin\Block\StopwatchBlock File Contains the StopwatchBlock code.
 */

namespace Drupal\stopwatch\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'StopwatchBlock' block.
 *
 * @Block(
 *  id = "stopwatch_block",
 *  admin_label = @Translation("Stopwatch block"),
 * )
 */
class StopwatchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'stopwatch_block',
      '#attached' => [
        'library' => ['stopwatch/stopwatch_block'],
      ],
    ];
  }

}
